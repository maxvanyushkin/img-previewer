module img-previewer

go 1.13

require (
	github.com/coreos/etcd v3.3.3+incompatible
	github.com/heetch/confita v0.9.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/oliamb/cutter v0.2.2
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.4.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
