FROM golang:latest

WORKDIR /usr/src/app
COPY . .
RUN go build -o imgresizer cmd/main.go
RUN cp configs/config.json config.json

EXPOSE 8883

CMD ["./imgresizer"]




