 .PHONY: up down restart test

init:
	@echo "[x] Loading dependencies..."
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.24.0
	go get -u golang.org/x/lint/golint
	go mod download
	@echo  "-> Done."

lint:
	@echo "[x] Running the basic linter GO VET"
	go vet ./...
	@echo  "-> Done. But we do NOT trust in GO VET, so we are gonna use GOLINT"

	@echo "[x] Running GOLINT..."
	golint
	@echo  "-> Done."

	@echo "[x] Running GOLANGCI-LINT..."
	golangci-lint run
	@echo  "-> Done."

test:
	@echo "[x] Running unit tests..."
	go test ./internal/http -race -count 100
	go test ./internal/domain/resizer
	@echo  "-> Done."

clean:
	@echo "[x] Cleaning up the previous build result"
	rm -r -f ./build
	@echo  "-> Done."

build:

	@echo  "[x] Building the project..."
	go build -o build/img-previewer cmd/main.go
	@echo  "-> Done"

	@echo "[x] Copying configuration files to the their own binaries"
	cp configs/config.json build/config.json
	@echo  "-> Done"

run:
	./build/img-previewer --config build/config.json

docker-run:
	docker-compose -f ./deploy/docker-compose.yml up

run-e2e:
	docker-compose -f ./deploy/docker-compose.tests.yml up --build --abort-on-container-exit



