package domain

type WorkItem struct {
	Url      string
	OutputCh chan<- interface{}
	Width    int32
	Height   int32
}
