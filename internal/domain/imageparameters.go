package domain

type ImageParameters struct {
	Width  int32
	Height int32
	Url    string
}
