package resizer

import (
	"bytes"
	"errors"
	ext "github.com/nfnt/resize"
	"github.com/oliamb/cutter"
	"image"
	"image/jpeg"
	_ "image/jpeg"
)

func Resize(sourceImage []byte, desiredWidth int32, desiredHeight int32) ([]byte, error) {
	img, format, err := image.Decode(bytes.NewReader(sourceImage))
	if err != nil {
		return nil, err
	}

	resized, err := resizeInternal(img, desiredWidth, desiredHeight)
	if err != nil {
		return nil, err
	}

	if format == "jpeg" {
		imageBuffer := new(bytes.Buffer)
		err = jpeg.Encode(imageBuffer, resized, nil)
		if err != nil {
			return nil, err
		}
		return imageBuffer.Bytes(), nil
	}

	return nil, errors.New("format is not supported")
}

func resizeInternal(srcImg image.Image, desiredWidth int32, desiredHeight int32) (image.Image, error) {
	sourceWidth := int32(srcImg.Bounds().Max.X)
	sourceHeight := int32(srcImg.Bounds().Max.Y)

	isAspectFactorTheSame := (sourceWidth*100)/(sourceHeight*100) == (desiredWidth*100)/(desiredHeight*100)
	if isAspectFactorTheSame {
		resized := ext.Resize(uint(desiredWidth), uint(desiredHeight), srcImg, ext.NearestNeighbor)
		return resized, nil
	}

	widthScaleFactor := float32(desiredWidth) / float32(sourceWidth)
	heightScaleFactor := float32(desiredHeight) / float32(sourceHeight)

	var targetWidth int32
	var targetHeight int32
	if heightScaleFactor < widthScaleFactor {
		targetWidth = desiredWidth
		targetHeight = int32(float32(sourceHeight) * widthScaleFactor)
	} else {
		targetHeight = desiredHeight
		targetWidth = int32(float32(sourceWidth) * heightScaleFactor)
	}

	resized := ext.Resize(uint(targetWidth), uint(targetHeight), srcImg, ext.Bicubic)
	return cutter.Crop(resized, cutter.Config{
		Width:  int(desiredWidth),
		Height: int(desiredHeight),
		Mode:   cutter.Centered,
	})
}
