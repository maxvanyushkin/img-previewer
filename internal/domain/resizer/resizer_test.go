package resizer

import (
	"github.com/stretchr/testify/assert"
	assets "img-previewer/assets/test"
	"testing"
)

func TestResize50_50(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(50), int32(50))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset50x50)
}

func TestResize200_700(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(200), int32(700))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset200x700)
}

func TestResize256_126(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(256), int32(126))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset256x126)
}

func TestResize333_666(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(333), int32(666))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset336x666)
}

func TestResize500_500(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(500), int32(500))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset500x500)
}

func TestResize1024_252(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(1024), int32(252))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset1024x254)
}

func TestResize2000_1000(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(2000), int32(1000))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAsset2000x1000)
}

func TestResizeExtraLarge(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(5000), int32(5000))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAssetExtraLarge)
}

func TestResizeExtraSmall(t *testing.T) {
	image, err := Resize(assets.TestAssetOriginal, int32(5), int32(5))
	assert.Nil(t, err)
	assert.Equal(t, image, assets.TestAssetExtraSmall)
}
