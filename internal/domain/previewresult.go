package domain

import (
	"net/http"
)

type ErrorPreviewResult struct {
	Error error
}

type SuccessPreviewResult struct {
	Image  []byte
	Header http.Header
}
