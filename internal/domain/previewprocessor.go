package domain

import (
	"crypto/md5"
	"encoding/hex"
	log "github.com/sirupsen/logrus"
	"img-previewer/internal/cache"
	"img-previewer/internal/domain/resizer"
	"img-previewer/internal/imageloader"
	"io/ioutil"
	"net/url"
	"strings"
)

type PreviewProcessor struct {
	cache cache.Cache
}

func NewPreviewProcessor(cache cache.Cache) *PreviewProcessor {
	return &PreviewProcessor{
		cache: cache,
	}
}

func (p *PreviewProcessor) ProcessRequest(item *WorkItem) {
	linkHash := p.getLinkHash(item)
	image, err := p.cache.Get(linkHash)
	if err != nil {
		p.completeWithError(item, err)
		return
	}

	if image != nil {
		p.completeWithSuccess(item, image)
	} else {
		image, err = p.loadAndPutToCache(item, linkHash)
		if err != nil {
			p.completeWithError(item, err)
			return
		}

		p.completeWithSuccess(item, image)
	}
}

func (p *PreviewProcessor) completeWithSuccess(item *WorkItem, image []byte) {
	successResult := SuccessPreviewResult{
		Image: image,
	}
	item.OutputCh <- interface{}(successResult)
}

func (p *PreviewProcessor) completeWithError(item *WorkItem, err error) {
	errorResult := ErrorPreviewResult{Error: err}
	item.OutputCh <- interface{}(errorResult)
}

func (p *PreviewProcessor) getLinkHash(item *WorkItem) string {
	fullKey := strings.Join([]string{item.Url, string(item.Height), string(item.Width)}, "-")
	md5Hasher := md5.New()
	_, _ = md5Hasher.Write([]byte(fullKey))
	hash := hex.EncodeToString(md5Hasher.Sum(nil))
	log.Infof("full key %v has been shortened to %v", fullKey, hash)
	return hash
}

func (p *PreviewProcessor) loadAndPutToCache(item *WorkItem, linkHash string) ([]byte, error) {
	addr, err := url.Parse(item.Url)
	if err != nil {
		return nil, err
	}

	ldr := imageloader.New(addr)
	defer ldr.Close()

	r, err := ldr.Do()
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}

	image, _ := ioutil.ReadAll(r)
	image, err = resizer.Resize(image, item.Width, item.Height)
	if err != nil {
		return nil, err
	}

	err = p.cache.Put(linkHash, image)
	return image, err
}
