package middlewares

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func LogRequest(h func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Infof("Request started...")
		startTime := time.Now()
		h(w, r)
		stopTime := time.Now()
		t := stopTime.UnixNano() - startTime.UnixNano()
		log.Infof("Request is done in %v ms", t)
	}
}
