package middlewares

import (
	"fmt"
	"net/http"
)

func HandlePanic(h func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println(err)
			}
		}()
		h(w, r)
	}
}
