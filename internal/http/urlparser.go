package http

import (
	"errors"
	"fmt"
	"img-previewer/internal/domain"
	"net/url"
	"path"
	"strconv"
	"strings"
)

func GetImageParametersFromPath(s string) (*domain.ImageParameters, error) {
	if s == "" {
		return nil, errors.New("the parameters string is empty")
	}

	segments := strings.Split(s, "/")
	if len(segments) < 5 {
		return nil, errors.New("string is invalid")
	}

	width, err := strconv.Atoi(segments[2])
	if err != nil {
		return nil, fmt.Errorf("the width must be a digit (passed value is %v)", width)
	}

	if width <= 0 {
		return nil, fmt.Errorf("the width must be greater than 0 (passed value is %v)", width)
	}

	height, err := strconv.Atoi(segments[3])
	if err != nil {
		return nil, fmt.Errorf("the height must be a digit (passed value is %v)", height)
	}

	if height <= 0 {
		return nil, fmt.Errorf("the height must be greater than 0 (passed value is %v)", height)
	}

	imageUrl := path.Join(segments[4:]...)

	imageUrl = fmt.Sprintf("http://%v", imageUrl)

	u, err := url.ParseRequestURI(imageUrl)
	if err != nil {
		return nil, errors.New("string is invalid")
	}

	if u.Scheme != "" && u.Scheme != "http" && u.Scheme != "https" {
		return nil, fmt.Errorf("the specified schema %v is not supported", u.Scheme)
	}

	return &domain.ImageParameters{
		Width:  int32(width),
		Height: int32(height),
		Url:    imageUrl,
	}, nil
}
