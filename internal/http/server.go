package http

import (
	"bytes"
	"errors"
	log "github.com/sirupsen/logrus"
	"img-previewer/internal/domain"
	m "img-previewer/internal/http/middlewares"
	"io"
	"net/http"
	"time"
)

type imagePreviewServer struct {
	processor *domain.PreviewProcessor
}

func NewImagePreviewerServer(processor *domain.PreviewProcessor) *imagePreviewServer {
	return &imagePreviewServer{
		processor: processor,
	}
}

func (i *imagePreviewServer) Listen(bindAddress string) error {
	log.Infof("Starting listener on %v", bindAddress)
	chain := m.HandlePanic(m.LogRequest(i.handle))
	http.HandleFunc("/", chain)
	return http.ListenAndServe(bindAddress, nil)
}

func (i *imagePreviewServer) handle(w http.ResponseWriter, r *http.Request) {
	outputCh := make(chan interface{})
	params, _ := GetImageParametersFromPath(r.RequestURI)
	workItem := &domain.WorkItem{
		Url:      params.Url,
		Width:    params.Width,
		Height:   params.Height,
		OutputCh: outputCh,
	}
	log.Infof("has gotten a work item: [%v-%v] %v", params.Width, params.Height, params.Url)

	timer := time.NewTimer(time.Second * 30)
	go i.processor.ProcessRequest(workItem)
	select {
	case previewResult := <-outputCh:
		i.writePreviewResult(w, previewResult)
	case <-timer.C:
		writeErrorResponse(w, errors.New("timeout"))
	case <-r.Context().Done():
		log.Error("request context has done")
	}
}

func (i *imagePreviewServer) writePreviewResult(w http.ResponseWriter, previewResult interface{}) {
	switch v := previewResult.(type) {
	case domain.SuccessPreviewResult:
		writeSuccessResponse(w, v)
	case domain.ErrorPreviewResult:
		writeErrorResponse(w, v.Error)
	}
}

func writeSuccessResponse(w http.ResponseWriter, previewResult domain.SuccessPreviewResult) {
	w.Header().Add("Content-Type", "image/jpeg")
	imgReader := bytes.NewReader(previewResult.Image)
	_, err := io.Copy(w, imgReader)
	logIfNotNil(err)
}

func writeErrorResponse(w http.ResponseWriter, e error) {
	_, err := w.Write([]byte(e.Error()))
	logIfNotNil(err)
}

func logIfNotNil(err error) {
	if err != nil {
		log.Errorf("During the response writing process, occurred an error: %v", err.Error())
	}
}
