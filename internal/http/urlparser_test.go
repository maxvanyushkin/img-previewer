package http

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseValidUrl(t *testing.T) {
	regularString := "/fill/200/700/www.audubon.org/sites/default/test_assets/a1_1902_16_barred-owl_sandra_rothenberg_kk.jpg"
	imgParams, err := GetImageParametersFromPath(regularString)

	assert.Nil(t, err)
	assert.NotNil(t, imgParams)

	assert.Equal(t, imgParams.Url, "http://www.audubon.org/sites/default/test_assets/a1_1902_16_barred-owl_sandra_rothenberg_kk.jpg")
	assert.Equal(t, imgParams.Width, int32(200))
	assert.Equal(t, imgParams.Height, int32(700))
}

func TestParseUrlHavingInvalidWidth(t *testing.T) {
	regularString := "/fill/-300/200/www.audubon.org/sites/default/test_assets/a1_1902_16_barred-owl_sandra_rothenberg_kk.jpg"
	_, err := GetImageParametersFromPath(regularString)

	assert.NotNil(t, err)
}

func TestParseUrlHavingInvalidHeight(t *testing.T) {
	regularString := "/fill/300/-2200/www.audubon.org/sites/default/test_assets/a1_1902_16_barred-owl_sandra_rothenberg_kk.jpg"
	_, err := GetImageParametersFromPath(regularString)

	assert.NotNil(t, err)
}

func TestParseUrlHavingInvalidScheme(t *testing.T) {
	regularString := "/fill/-300/200/ws://www.audubon.org/sites/default/test_assets/a1_1902_16_barred-owl_sandra_rothenberg_kk.jpg"
	_, err := GetImageParametersFromPath(regularString)
	assert.NotNil(t, err)
}
