package config

type Config struct {
	LRUCacheSize  int64  `config:"lrucachesize"`
	Address       string `config:"address"`
	CacheType     string `config:"cachetype"`
	FileCachePath string `config:"filecachepath"`
}
