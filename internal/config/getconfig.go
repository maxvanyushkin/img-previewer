package config

import (
	"context"
	"flag"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/env"
	"github.com/heetch/confita/backend/file"
	"img-previewer/internal/errors"
	"os"
	"path"
)

const defaultConfigFileName = "config.json"

func GetConfig() (*Config, error) {
	configFileName := flag.String("config", defaultConfigFileName, "settings file")
	flag.Parse()

	_, err := os.Stat(*configFileName)
	if err != nil {
		return nil, errors.NewConfigFailed(err.Error())
	}

	wd, err := os.Getwd()
	if err != nil {
		return nil, errors.NewConfigFailed(err.Error())
	}

	configFullPath := path.Join(wd, *configFileName)
	loader := confita.NewLoader(env.NewBackend(), file.NewBackend(configFullPath))

	config := &Config{}
	err = loader.Load(context.Background(), config)
	if err != nil {
		return nil, errors.NewConfigFailed(err.Error())
	}

	return config, nil
}
