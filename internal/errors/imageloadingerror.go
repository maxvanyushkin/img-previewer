package errors

import (
	"fmt"
	"net/url"
)

type imageLoadingError struct {
	url     *url.URL
	message string
}

func NewImageLoadingError(url *url.URL, message string) *imageLoadingError {
	return &imageLoadingError{
		url:     url,
		message: message,
	}
}

func (i imageLoadingError) Error() string {
	return fmt.Sprintf("occurred an error %v during loading an image by the link %v", i.message, i.url)
}
