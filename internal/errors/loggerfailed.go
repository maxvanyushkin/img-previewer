package errors

type LoggerFailed struct {
	message string
}

func NewLoggerFailed(message string) *LoggerFailed {
	return &LoggerFailed{
		message: message,
	}
}

func (c LoggerFailed) Error() string {
	return c.message
}
