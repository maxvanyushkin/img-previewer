package errors

type ConfigFailed struct {
	message string
}

func NewConfigFailed(message string) *ConfigFailed {
	return &ConfigFailed{
		message: message,
	}
}

func (c ConfigFailed) Error() string {
	return c.message
}
