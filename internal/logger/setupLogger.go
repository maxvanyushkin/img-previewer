package logger

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"img-previewer/internal/errors"
	"io"
	"os"
	"path"
)

const defaultLogPath = "logs/output.log"

const defaultLogLevel = "INFO"

func Setup() error {
	wd, err := os.Getwd() //nolint:ineffassign
	if err != nil {
		return errors.NewLoggerFailed(fmt.Sprintf("unable to get working directory, occurred an error: %v", err.Error()))
	}

	logsDir := path.Dir(defaultLogPath)
	_, err = os.Stat(logsDir)
	if os.IsNotExist(err) {
		err := os.MkdirAll(logsDir, os.ModePerm)
		if err != nil {
			return errors.NewLoggerFailed("Unable to create folder for logs, please check the logger path")
		}
	}

	f, err := os.OpenFile(path.Join(wd, defaultLogPath), os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	log.SetOutput(os.Stdout)
	if err == nil {
		log.SetOutput(io.MultiWriter(os.Stdout, f))
	} else {
		log.Info("Failed to log to file, using default stderr")
	}

	level, err := log.ParseLevel(defaultLogLevel)
	if err != nil {
		log.Infof("Unknown log level %v", defaultLogLevel)
	}
	log.SetLevel(level)
	return nil
}
