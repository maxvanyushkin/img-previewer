package cache

import (
	"container/list"
	"sync"
	"time"
)

type Cache interface {
	Put(key string, value []byte) error
	Get(key string) ([]byte, error)
}

type DataProvider interface {
	Cache
	ItemsCount() int64
	Delete(key string) error
	PreloadExistingKeys() []string
}

type syncCacheWrapper struct {
	dataProvider DataProvider
	sizeLimit    int64
	cacheMutex   sync.RWMutex
	lruMapMutex  sync.RWMutex
	lruItemsMap  map[string]*lruMapItem
	lruItemsList *list.List
}

type lruListItem struct {
	timestamp  *int64
	key        string
	lruMapItem *lruMapItem
}

type lruMapItem struct {
	timestamp   *int64
	lruListItem *list.Element
}

func New(dataProvider DataProvider, limit int64) Cache {
	existingKeys := dataProvider.PreloadExistingKeys()
	lruInfo := make(map[string]*lruMapItem)
	lruList := list.New()

	for _, key := range existingKeys {
		timestamp := time.Now().UnixNano()
		createLinkedLruItems(timestamp, key, lruList, lruInfo)
	}

	return &syncCacheWrapper{
		dataProvider: dataProvider,
		sizeLimit:    limit,
		lruItemsMap:  lruInfo,
		lruItemsList: lruList,
	}
}

func (c *syncCacheWrapper) Put(key string, value []byte) error {
	c.cacheMutex.Lock()
	defer c.cacheMutex.Unlock()

	err := c.truncateIfNeed()
	if err != nil {
		return err
	}

	if c.lruItemsMap[key] == nil {
		createLinkedLruItems(0, key, c.lruItemsList, c.lruItemsMap)
	}

	err = c.dataProvider.Put(key, value)
	if err == nil {
		c.updateLruInfoEntry(key)
	}

	return err
}

func (c *syncCacheWrapper) Get(key string) ([]byte, error) {
	c.cacheMutex.RLock()
	defer c.cacheMutex.RUnlock()

	v, err := c.dataProvider.Get(key)
	if err != nil {
		return nil, err
	}

	if v == nil {
		return nil, nil
	}

	if c.lruItemsMap[key] == nil {
		c.addLruInfoEntry(key)
	}

	c.updateLruInfoEntry(key)
	return v, err
}

func (c *syncCacheWrapper) addLruInfoEntry(key string) {
	var defaultValue int64 = 0
	c.lruMapMutex.Lock()
	createLinkedLruItems(defaultValue, key, c.lruItemsList, c.lruItemsMap)
	c.lruMapMutex.Unlock()
}

func (c *syncCacheWrapper) updateLruInfoEntry(key string) {
	timestamp := time.Now().UnixNano()
	c.lruItemsMap[key].timestamp = &timestamp
	c.lruItemsList.MoveToBack(c.lruItemsMap[key].lruListItem)
}

func (c *syncCacheWrapper) truncateIfNeed() error {
	if c.dataProvider.ItemsCount() >= c.sizeLimit {
		rarelyUsedListElement := c.lruItemsList.Front()
		lruListItem := rarelyUsedListElement.Value.(*lruListItem)
		err := c.dataProvider.Delete(lruListItem.key)
		if err != nil {
			return err
		}
		c.lruMapMutex.Lock()
		delete(c.lruItemsMap, lruListItem.key)
		c.lruItemsList.Remove(rarelyUsedListElement)
		c.lruMapMutex.Unlock()
		return nil
	}
	return nil
}

func createLinkedLruItems(timestamp int64, key string, lruList *list.List, lruInfo map[string]*lruMapItem) {
	listItem := &lruListItem{
		timestamp:  &timestamp,
		key:        key,
		lruMapItem: nil,
	}

	element := lruList.PushBack(listItem)
	mapItem := &lruMapItem{
		timestamp:   &timestamp,
		lruListItem: element,
	}
	listItem.lruMapItem = mapItem
	lruInfo[key] = mapItem
}
