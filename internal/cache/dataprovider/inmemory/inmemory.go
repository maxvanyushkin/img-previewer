package inmemory

type InMemoryCache struct {
	Items map[string][]byte
}

func (c *InMemoryCache) PreloadExistingKeys() []string {
	return []string{}
}

func New() *InMemoryCache {
	return &InMemoryCache{
		Items: make(map[string][]byte),
	}
}

func (c *InMemoryCache) Put(key string, value []byte) error {
	c.Items[key] = value
	return nil
}

func (c *InMemoryCache) Delete(key string) error {
	delete(c.Items, key)
	return nil
}

func (c *InMemoryCache) Get(key string) ([]byte, error) {
	return c.Items[key], nil
}

func (c *InMemoryCache) ItemsCount() int64 {
	return int64(len(c.Items))
}
