package fs

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"sync/atomic"
)

type FileSystemDataProvider struct {
	path         string
	itemsCount   *int64
	writeChannel chan<- interface{}
}

func New(path string) (*FileSystemDataProvider, error) {
	err := prepareFileSystem(path)
	if err != nil {
		return nil, err
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	itemsCount := int64(len(files))
	writeChannel := make(chan interface{})
	fs := &FileSystemDataProvider{
		path:         path,
		writeChannel: writeChannel,
		itemsCount:   &itemsCount,
	}

	go func() {
		fs.flushToFileSystem(writeChannel)
	}()
	return fs, nil
}

func (c *FileSystemDataProvider) PreloadExistingKeys() []string {
	files, err := ioutil.ReadDir(c.path)
	if err != nil {
		return []string{}
	}

	existingKeys := make([]string, 0)
	for _, f := range files {
		existingKeys = append(existingKeys, f.Name())
	}
	return existingKeys
}

func (f *FileSystemDataProvider) Put(key string, value []byte) error {
	done := make(chan error)
	item := writeItemUnit{
		filename: key,
		data:     value,
		done:     done,
	}
	f.writeChannel <- item
	err := <-done
	return err
}

func (f *FileSystemDataProvider) Delete(key string) error {
	done := make(chan error)
	item := deleteItem{
		filename: key,
		done:     done,
	}
	f.writeChannel <- item
	err := <-done
	return err
}

func (f *FileSystemDataProvider) Get(key string) ([]byte, error) {
	b, err := ioutil.ReadFile(path.Join(f.path, key))
	if os.IsNotExist(err) {
		return nil, nil
	}

	return b, err
}

func (f *FileSystemDataProvider) ItemsCount() int64 {
	return atomic.LoadInt64(f.itemsCount)
}

func (f *FileSystemDataProvider) flushToFileSystem(dataSource <-chan interface{}) {
	for item := range dataSource {
		switch item := item.(type) {
		case writeItemUnit:
			filePath := path.Join(f.path, item.filename)
			err := ioutil.WriteFile(filePath, item.data, 0666)
			if err != nil {
				item.done <- fmt.Errorf("unable to create a file, occurred exception: %v", err.Error())
			} else {
				item.done <- nil
			}
			close(item.done)
		case deleteItem:
			err := os.Remove(item.filename)
			item.done <- err
			close(item.done)
		}
	}
}

func prepareFileSystem(path string) error {
	f, err := os.Stat(path)
	if os.IsNotExist(err) {
		err = os.MkdirAll(path, os.ModePerm)
		if err != nil {
			return err
		}
	}

	if f != nil && !f.IsDir() {
		return errors.New("it's a file, but we need a dir")
	}
	return nil
}
