package fs

type writeItemUnit struct {
	done     chan<- error
	filename string
	data     []byte
}
