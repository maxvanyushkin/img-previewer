package fs

type deleteItem struct {
	done     chan<- error
	filename string
}
