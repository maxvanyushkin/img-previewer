package cache

import (
	assert "github.com/stretchr/testify/assert"
	"img-previewer/internal/cache/dataprovider/inmemory"
	"testing"
)

func TestGetFromCacheWhenValueExists(t *testing.T) {
	originValue := []byte{1}
	c := New(&inmemory.InMemoryCache{
		Items: map[string][]byte{"key": originValue},
	}, 100)

	v, err := c.Get("key")
	assert.Nil(t, err)
	assert.Equal(t, v, originValue)
}

func TestGetFromCacheWhenValueDoesntExist(t *testing.T) {
	c := New(&inmemory.InMemoryCache{}, 100)
	v, err := c.Get("empty_key")
	assert.Nil(t, v)
	assert.Nil(t, err)
}

func TestPutToCacheWhenValueExists(t *testing.T) {
	originValue := []byte{1}
	newValue := []byte{2}
	dataProvider := &inmemory.InMemoryCache{
		Items: map[string][]byte{"key": originValue},
	}
	c := New(dataProvider, 100)

	err := c.Put("key", newValue)
	assert.Nil(t, err)
	assert.Equal(t, dataProvider.Items["key"], newValue)
}

func TestPutToCacheWhenValueDoesntExist(t *testing.T) {
	originValue := []byte{1}
	newValue := []byte{2}
	dataProvider := &inmemory.InMemoryCache{
		Items: map[string][]byte{"key": originValue},
	}
	c := New(dataProvider, 100)

	err := c.Put("key", newValue)
	assert.Nil(t, err)
	assert.Equal(t, dataProvider.Items["key"], newValue)
}

func TestWhenLastRecentUsageSizeIsExceeded(t *testing.T) {
	originValue := []byte{1}
	newValue := []byte{2}
	dataProvider := &inmemory.InMemoryCache{Items: map[string][]byte{}}
	c := New(dataProvider, 5)

	_ = c.Put("key1", originValue)
	_ = c.Put("key2", originValue)
	_ = c.Put("key3", originValue)
	_ = c.Put("key4", originValue)
	_ = c.Put("key5", originValue)

	err := c.Put("key100", newValue)
	assert.Nil(t, err)
	assert.Equal(t, dataProvider.Items["key100"], newValue)
	assert.Nil(t, dataProvider.Items["key1"], "the oldest key should have been truncated from the store")
}

func TestWhenLastRecentUsageSizeIsNotExceeded(t *testing.T) {
	originValue := []byte{1}
	newValue := []byte{2}
	dataProvider := &inmemory.InMemoryCache{Items: map[string][]byte{}}
	c := New(dataProvider, 5)

	_ = c.Put("key1", originValue)
	_ = c.Put("key1", originValue)
	_ = c.Put("key1", originValue)
	_ = c.Put("key1", originValue)
	_ = c.Put("key5", newValue)

	err := c.Put("key20", newValue)
	assert.Nil(t, err)
	assert.NotNil(t, dataProvider.Items["key1"], "the oldest key should not have been truncated from the store")
}
