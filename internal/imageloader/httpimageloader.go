package imageloader

import (
	"fmt"
	"img-previewer/internal/errors"
	"io"
	"net/http"
	"net/url"
	"strings"
)

type httpImageLoader struct {
	uri      *url.URL
	response io.ReadCloser
}

func New(url *url.URL) *httpImageLoader {
	return &httpImageLoader{
		uri: url,
	}
}

func (h *httpImageLoader) Do() (io.Reader, error) {
	resp, err := http.Get(h.uri.String())
	if err != nil {
		return nil, errors.NewImageLoadingError(h.uri, err.Error())
	}

	if resp.StatusCode != 200 {
		return nil, errors.NewImageLoadingError(h.uri, fmt.Sprintf("status code isn't ok (%v)", resp.StatusCode))
	}

	contentType := resp.Header.Get("Content-Type")
	if !strings.Contains(contentType, "image/jpeg") {
		return nil, errors.NewImageLoadingError(h.uri, fmt.Sprintf("the content-type header doesn't present an image (%v)", contentType))
	}

	// TODO: Validate image size
	h.response = resp.Body
	return resp.Body, nil
}

func (h *httpImageLoader) Close() {
	if h.response != nil {
		h.response.Close()
	}
}
