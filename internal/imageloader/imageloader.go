package imageloader

import "io"

type ImageLoader interface {
	Do() *io.Reader
	Close()
}
