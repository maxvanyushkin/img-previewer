package main

import (
	"bytes"
	"fmt"
	assets "img-previewer/assets/test"
	"net/http"
	"os"
	"time"
)

const resizerUrlEnvVar = "resizer_url"
const assetsServerUrlEnvVar = "asset_server_url"

var getAssetUrl func(width int, height int) string

func main() {
	time.Sleep(time.Second * 5)
	resizerUrl := os.Getenv(resizerUrlEnvVar)
	assetUrl := os.Getenv(assetsServerUrlEnvVar)
	fmt.Printf("the resizer url is %v \n", resizerUrl)
	fmt.Printf("the source asset url is %v \n", assetUrl)
	getAssetUrl = getAssetInRequiredSize(resizerUrl, assetUrl)

	Test200x700()
	Test333x666()
	Test2000x1000()
	println("Tests are ok. Congratulations.")
}

func Test200x700() {
	println("Testing 200 x 700 ... ")
	url := getAssetUrl(200, 700)
	println(url)
	r, err := http.Get(url)
	if err != nil {
		println(err.Error())
		os.Exit(-1)
	}

	defer r.Body.Close()
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(r.Body)
	if err != nil {
		println(err.Error())
		os.Exit(-1)
	}

	receivedBytes := buf.Bytes()
	areEqual := bytes.Equal(assets.TestAsset200x700, receivedBytes)
	if !areEqual {
		println("200x700 cropping and resizing doesn't work")
		os.Exit(-1)
	}
	println("Done.")
}

func Test333x666() {
	println("Testing 333 x 666 ... ")
	url := getAssetUrl(333, 666)
	println(url)
	r, err := http.Get(url)
	if err != nil {
		println(err.Error())
		os.Exit(-1)
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(r.Body)
	if err != nil {
		println(err)
		os.Exit(-1)
	}

	defer r.Body.Close()
	receivedBytes := buf.Bytes()
	areEqual := bytes.Equal(assets.TestAsset336x666, receivedBytes)
	if !areEqual {
		println("336x666 cropping and resizing doesn't work")
		os.Exit(-1)
	}
	println("Done.")
}

func Test2000x1000() {
	println("Testing 2000 x 1000 ... ")
	url := getAssetUrl(2000, 1000)
	println(url)
	r, err := http.Get(url)
	if err != nil {
		println(err)
		os.Exit(-1)
	}

	defer r.Body.Close()
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(r.Body)
	if err != nil {
		println(err.Error())
		os.Exit(-1)
	}

	receivedBytes := buf.Bytes()
	areEqual := bytes.Equal(assets.TestAsset2000x1000, receivedBytes)
	if !areEqual {
		println("2000x1000 cropping and resizing doesn't work")
		os.Exit(-1)
	}
	println("Done.")
}

func getAssetInRequiredSize(resizerUrl string, assetUrl string) func(width int, height int) string {
	return func(width int, height int) string {
		return fmt.Sprintf("%v/fill/%v/%v/%v", resizerUrl, width, height, assetUrl)
	}
}
