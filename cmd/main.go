package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"img-previewer/internal/cache"
	"img-previewer/internal/cache/dataprovider/fs"
	"img-previewer/internal/cache/dataprovider/inmemory"
	"img-previewer/internal/config"
	"img-previewer/internal/domain"
	server "img-previewer/internal/http"
	"img-previewer/internal/logger"
)

func main() {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatal(err.Error())
	}

	err = logger.Setup()
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Infoln("Application started.")
	log.Infof("Settings: LRU Cache Size %v", cfg.LRUCacheSize)
	if err != nil {
		log.Fatal(err.Error())
	}

	dataProvider, err := GetDataProvider(cfg)
	if err != nil {
		log.Fatal(err.Error())
	}

	c := cache.New(dataProvider, cfg.LRUCacheSize)
	srv := server.NewImagePreviewerServer(domain.NewPreviewProcessor(c))
	err = srv.Listen(cfg.Address)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func GetDataProvider(cfg *config.Config) (cache.DataProvider, error) {
	var dataProvider cache.DataProvider
	var err error
	switch cfg.CacheType {
	case cache.File:
		dataProvider, err = fs.New(cfg.FileCachePath)
		if err != nil {
			log.Fatal(err.Error())
		}
	case cache.Memory:
		dataProvider = inmemory.New()
	default:
		return nil, fmt.Errorf("unkown cache type %v", cfg)
	}
	return dataProvider, err
}
